LATEXMK=latexmk
PANDOC=pandoc
BIBTOOL=bibtool
keysM=rm

COMMON_OPTIONS=-s --metadata-file=pandoc/metadata.yaml -F pandoc-crossref --lua-filter=pandoc/sitefilter.lua
HTML_OPTIONS=-s --toc-depth=1 --toc --template=pandoc/template.html
LATEX_OPTIONS=-s --toc-depth=1 --template=pandoc/template.tex --pdf-engine=latexmk --biblatex --pdf-engine-opt=-pvc- --pdf-engine-opt=-lualatex
MARKDOWN_FLAVOUR=commonmark-fenced_divs-bracketed_spans-raw_html
#MARKDOWN_OPTIONS=--template=pandoc/template.tex --pdf-engine=latexmk --biblatex --pdf-engine-opt=-pvc- --pdf-engine-opt=-lualatex

cssdir    := assets/css
sourcecss := $(wildcard assets/css/*.css)
cssfiles  := $(notdir $(sourcecss))
css       := $(cssfiles:%.css=public/%.css)
bibdir    := public/bib
select_command = $(BIBTOOL) -r keep_bibtex -- 'select{$$key "$(key)"}' selected.bib -o $(bibdir)/$(key).bib

site: public/webpage.html public/index.html public/Julien_Ugon.pdf public/Julien_Ugon.md $(css)

public:
	mkdir public

public/index.html: input/index.html
	cp $< $@
	
public/webpage.html : input/webpage.md selected.bib pandoc/template.html public public/JulienUgon.jpg
	$(PANDOC) $(HTML_OPTIONS) $(COMMON_OPTIONS) -t html $< -o $@ 

public/Julien_Ugon.pdf :  input/webpage.md pandoc/template.tex public 
	$(PANDOC) $(LATEX_OPTIONS) $(COMMON_OPTIONS) -t latex $< -o $@
	
public/Julien_Ugon.md :  input/webpage.md public
	$(PANDOC) $(MARKDOWN_OPTIONS) $(COMMON_OPTIONS) -t $(MARKDOWN_FLAVOUR) $< -o $@

public/JulienUgon.jpg: JulienUgon.jpg
	convert $< -crop 1172x1172+50+304 -resize 40% $@
	
CV.tex :  input/webpage.md publications.bib pandoc/template.tex
	$(PANDOC) $(LATEX_OPTIONS) $(COMMON_OPTIONS) -t latex $< -o $@

$(bibdir): public
	mkdir $(bibdir)

bib: selected.bib
	@$(eval citekeys = $(shell bibtool -- keep.field{"$key"} selected.bib | sed "s/@\w*//;s/[{}]//"))
	for key in $(citekeys); do \
          $(BIBTOOL) -r keep_bibtex -- 'select{$$key "'"$${key}"'"}' $< -o $(bibdir)/$${key}.bib; \
        done

$(citekeys): $(bibdir)/%.bib: keys
	@echo $@

selected.bib: publications.bib
	$(BIBTOOL) -r cv.rsc $< > $@

$(cssfiles): public

$(css): public/%.css : $(cssdir)/%.css
	cp $< $@

clean:
	-$(RM) public/*
	-$(RM) selected.bib
	-rmdir public

.PHONY: bib keys clean

