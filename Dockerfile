FROM pandoc/latex:2.18

# Update Alpine and install tools
RUN apk upgrade --update #&& apk add --no-cache --update bash

# Install additional fonts
RUN apk add --no-cache --update git curl gcc make musl-dev imagemagick

# Install additional Latex packages
RUN tlmgr update --self
RUN tlmgr update --all
RUN tlmgr install latexmk\
                  sourcesanspro\
                  sourceserifpro\
                  biblatex-ext\
                  koma-script

# Install bibtool
RUN curl -O http://www.gerd-neugebauer.de/software/TeX/BibTool/BibTool-2.68.tar.gz
RUN tar zxvf BibTool-2.68.tar.gz
WORKDIR BibTool
RUN ./configure
RUN make
RUN make install

RUN apk del --no-cache --update git curl gcc musl-dev

WORKDIR /data
ENTRYPOINT ["/usr/bin/make"]
