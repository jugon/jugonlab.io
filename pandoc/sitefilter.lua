local oldTitle = nil
local bibheads = {["article-journal"]={pandoc.Str("Journal"),pandoc.Space(),pandoc.Str("Articles")},
["chapter"]={pandoc.Str("Book"),pandoc.Space(),pandoc.Str("Chapters")},
["paper-conference"]={pandoc.Str("Conference"),pandoc.Space(),pandoc.Str("Papers")},
["report"]={pandoc.Str("Technical"),pandoc.Space(),pandoc.Str("Reports")},
["manuscript_no"]={pandoc.Str("In"),pandoc.Space(),pandoc.Str("Preparation")},
["manuscript"]={pandoc.Str("Preprints")},
["thesis"]={pandoc.Str("PhD"),pandoc.Space(),pandoc.Str("Thesis")}}

local bibfile = ""

local function getBibfileName(Meta)
  local biblio = Meta["bibliography"]
  for k=1,#biblio do
    bibname = biblio[k].text
    bibfile = bibfile .. bibname .. " "
  end
end

-- Only keep images in html
if FORMAT ~= "html" and FORMAT ~= "html5" then
    function Image(im)
      return {}
    end
end

-- In html, separate sections using "<section>" tags, and enclose the document in a "<main>" tag.
function setup_document(doc)
  if FORMAT == "html" or FORMAT == "html5" then
    local sections = pandoc.utils.make_sections(false, nil, doc.blocks)
    table.insert(sections,1,pandoc.RawBlock("html","<main>"))
    table.insert(sections,pandoc.RawBlock("html","</main>"))
    return pandoc.Pandoc(sections, doc.meta)
  end
end

function includeSections(div)
  if div.classes:includes("section") then
    return {pandoc.RawBlock("html","<section>"), div, pandoc.RawBlock("html","</section>")}
  else
    return {div}
  end
end

-- Put a horizontal line in front of the headers in markdown and plain text files:
if FORMAT == "markdown" or FORMAT == "plain" or FORMAT == "commonmark" then
  function Header(header)
    if header.level == 1  then
      return {pandoc.HorizontalRule(),header}
    end
  end
end

-- For links to arxiv in the bibliography, replace the text with just "Arxiv" and the number.
function fixArxivLinks(link)
  if link.target:sub(1,22) == "https://arxiv.org/abs/" then
    link.content  = {pandoc.Str("arXiv:"),pandoc.Space(" "),pandoc.Str(link.target:sub(23))}
  end
  local txt = pandoc.utils.stringify(link.content)
  if txt:sub(1,16) == "https://doi.org/" then
    return {}
  end

  return {link}
end

-- Include headers in the bibliography.
function isBlockType(span)
  return span.t == 'Span' and span.classes:includes("csl-block") and #(span.content) == 1 and span.content[1].t == 'Str'
end

function findSpanType(inlines)
  if inlines and #(inlines)>1 then
    local spans = inlines:filter(function(x) return x.t == 'Span' end)
    local title = spans:find_if(isBlockType)
    if title then
      return title.content[1].text
    else
      for k,s in pairs(spans) do
        local title = findSpanType(s.content)
        if title then
          return title
        end
      end
    end
  end
  return nil
end

function fixBibliography(div)
  if div.classes:includes("csl-entry") then
    local para = div.content:filter(function(x) return x.t == "Para" end)
    local title = nil
    for k,p in pairs(para) do
      title=findSpanType(p.content)
      if title then
        break
      end
    end
    if title then
      div = pandoc.walk_block(div,{Span = function(span) return (isBlockType(span) and span.content[1].text == title and {} or {span}) end})
    end
    if title and title ~= oldTitle then
      oldTitle = title
      return {pandoc.Header(2,bibheads[title]),div}
    else
      return {div}
    end
  end
end


function getContainer(div)
  if div.t ~= "Div" then return false end
  return div.classes and div.classes:includes("csl-right-inline")
end

-- Include the bibtex entry at the end.
function addBibtexEntry(div)
  if div.classes:includes("csl-entry") then
    local id=div.identifier:match("^ref%-(.*)")
    local cmd = "bibtool -r keep_bibtex -- 'select {$key \"" .. id .. "$\"}' " .. bibfile
    local f = io.popen(cmd,"r")
    local bibentry = f:read('*a'):match("^%s*(.*)") -- drop the whitespace at the start.
    f:close()
    if bibentry ~= "" then
      local cb = pandoc.CodeBlock(bibentry,{"cb-"..id,{"bibtex"},{}})
--      local cb = pandoc.CodeBlock(bibentry,{"cb-"..id,{"bibtex_source"},{}})
      local clip = pandoc.RawBlock("html",'<button onclick="bibtex_copy(this.previousElementSibling)" title="Copy bibtex">🗐</button>')
--      local download = pandoc.Link({pandoc.Str"⭳"},"bib/"..id..".bib","Download bib entry", {class = {'bib_download'}})
      local download = pandoc.Link({pandoc.Str"⭳"},"bib/"..id..".bib","Download bib entry", {"",{"bib_dl"},{}})
--      local download = pandoc.Link({pandoc.Str"↓⭳"},"bib/"..id..".bib","Download bib entry")
--      local clip = pandoc.RawBlock("html","<button onclick="bibtex_copy(this.parentNode)">⎘</button>")
--      div.content:insert(pandoc.Div(cb,{"",{"bibtex"},{}}))
      div.content:insert(cb)
      div.content:insert(clip)
      div.content:insert(download)
      return div
    end
  end
end

-- Put the bibliographies into lists.
function findBibliography(div)
  if div.classes:includes("csl-bib-body") then
    local blocks = pandoc.List()
    local lists = pandoc.List()
    local i = 1
    local bibnumber = 1
    while(div.content[i]) do
      while (div.content[i] and div.content[i].t ~= "Div") do
        if(div.content[i]) then
          blocks:insert(div.content[i])
        end
        i = i+1
      end
      local newlist = pandoc.List()
      local newAttr = pandoc.ListAttributes(bibnumber)
      while (div.content[i] and div.content[i].t == "Div") do
        newlist:insert(div.content[i])
        i = i+1
        bibnumber = bibnumber + 1
      end
      blocks:insert(pandoc.OrderedList(newlist, newAttr))
    end
    return pandoc.Div(blocks,div.attr)
  else
    return div
  end
end

-- Filter bibliography - Not currently in use. Untested.
local function filterBibliography(doc)
    local mycit = pandoc.Citation("sukhorukova.ea:nd","NormalCitation")
    local citList = pandoc.Cite({},{mycit})
    local refs_div = pandoc.Div({}, pandoc.Attr('refs'))
    local test = {pandoc.Para({citList}),refs_div}
    local ptest = pandoc.Pandoc(test,doc.meta)
end


-- Replace the CSL references with a call to biblatex \printbibliography.
function replaceRefSection(div)
  if div.classes:includes("csl-bib-body") then
    return pandoc.RawBlock("latex","\
  \\printbibliography[type=unpublished,heading=subbibnumbered,title={Preprints},resetnumbers=true]\n\
  \\printbibliography[type=article,heading=subbibnumbered,title={Journal Articles},resetnumbers=false]\n\
  \\printbibliography[type=incollection,heading=subbibnumbered,title={Book Chapters},resetnumbers=false]\n\
  \\printbibliography[type=inproceedings,heading=subbibnumbered,title={Peer-reviewed Conference Publications},resetnumbers=false]")
  end
end

-- Run citeproc.
local function getBibliography(doc)
  tdoc = pandoc.utils.run_json_filter(
      doc,
      'pandoc',
      {'--from=json', '--to=json', '--citeproc', '--quiet'}
    )
  if FORMAT ~= "latex" then
    return tdoc
  else
    -- if the format is latex, then we replace the refs section with a printbibliography:
    tblocks = pandoc.walk_block(pandoc.Div(tdoc.blocks),{Div = replaceRefSection})
    return pandoc.Pandoc(tblocks.content,tdoc.meta)
  end
end

return {
  {Image = Image},
  {Meta = getBibfileName},
  {Pandoc = getBibliography},
  {Div = fixBibliography},
  {Div = addBibtexEntry},
  {Div = findBibliography},
  {Pandoc = setup_document},
  {Link = fixArxivLinks},
  {Div = includeSections},
  {Header = Header}
}
