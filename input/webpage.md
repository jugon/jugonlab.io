---
title: Julien Ugon
author: Julien Ugon
keywords: [Ugon,publications, academic,optimisation, CV]
description: Julien Ugon's webpage.
affiliation: School of IT, Deakin University
bibliography: selected.bib
csl: julien.csl
nocite: '@*'
lang: en
link-citations: true
fontsize: 11pt
papersize: a4
classoption: [oneside,final,DIV=20,parskip=half*]
hyperrefoptions: [unicode,bookmarks,linkcolor=black, urlcolor=blue!50!black, colorlinks, breaklinks]
biblatexoptions: [style=ext-numeric-comp,articlein=false,sorting=ydnt,maxnames=99,defernumbers=true,giveninits=true,language=australian,date=year,isbn=false]
mainfontoptions: [Ligatures=TeX,Mapping=tex-text]
sansfontoptions: [Ligatures=TeX,Mapping=tex-text]
headfont: "\\sffamily"
titlefont: "\\fontsize{60}{72.0}\\selectfont"
subtitlefont: "\\fontsize{13.2}{20}\\selectfont"
sectionfont: "\\Large\\scshape"
subsectionfont: "\\itshape\\large"
biblio-style: numeric-comp
documentclass: scrartcl
csquotes: true
---


# About

![](JulienUgon.jpg){#fig:julien}

I am a researcher in mathematics. My research interests are nonsmooth
optimisation, approximation theory, and their applications.

I obtained my PhD in 2005, under the guidance of Prof. Alexander
Rubinov. I then worked at the University of Ballarat/Federation
University for 12 years, first as a research fellow, and then as a
lecturer in mathematics. I am now with the school of Information 
Technology at Deakin University. I am also a honorary research fellow with CIAO at Federation University.

# Research

## Research Projects

Here are some projects I have worked on:

1. Applications of optimisation to approximation theory. Funded by
    **ARC Discovery grant DP180100602.** See [@sukhorukova.ea:2021;@díaz-millán.ea:2022;@díaz-millán.ea:2022:1;@aghili.ea:2021;@peiris.ea:2021;@sukhorukova.ea:2022;@crouzeix.ea:2017;@crouzeix.ea:2020;@sukhorukova.ea:2016;@sukhorukova.ea:2017;@sukhorukova.ea:2010]
2. Optimization Algorithms. See [@bagirov.ea:2006;@beliakov.ea:2007;@bagirov.ea:2010;@bagirov.ea:2011:3;@bagirov.ea:2010:1;@rubinov.ea:2003;@díaz-millán.ea:nd]
3. Data Analysis. See [@bagirov.ea:2005;@rubinov.ea:2005;@bagirov.ea:2014;@bagirov.ea:2014:1;@bagirov.ea:2015;@bagirov.ea:2013;@bagirov.ea:2005:1;@bagirov.ea:2004;@rubinov.ea:2006;@ugon:2006;@bagirov.ea:2011;@bagirov.ea:2011:1;@bagirov.ea:2011:2;@bagirov.ea:2013:1;@rubinov.ea:2010;@bagirov.ea:2009;@bagirov.ea:2009:1;@soukhoroukova.ea:2003;@rubinov.ea:2004;@bagirov.ea:2016;@bagirov.ea:2018]
4. Discrete Geometry (Graphs of Polytopes). See [@pineda-villavicencio.ea:2018;@doolittle.ea:2019;@pineda-villavicencio.ea:2019;@nevo.ea:2020;@pineda-villavicencio.ea:2020;@bui.ea:2021;@bui.ea:2024;@jørgensen.ea:2022;@pilaud.ea:2023]
5. Applications of Optimisation.
   - Telecommunication Networks: Facility Location [@ugon.ea:2007;@kouhbor.ea:2005;@kouhbor.ea:2005:1;@kouhbor.ea:2004;@koubor.ea:2006]; Network Monitoring [@jia.ea:2003]
   - Scheduling Probems [@sukhorukova.ea:2009]
   - Sustainability Analysis [@lenzen.ea:2010;@geschke.ea:2019]
   - EEG signal analysis: [@sukhorukova.ea:2010:1]
   - Water Network planning [@bagirov.ea:2008]


## Research Funding

ARC Discovery Project DP180100602
: *An optimisation-based framework for non-classical Chebyshev approximation*

NeCTAR Development Grant (2012)
: *Industrial Ecology Virtual Laboratory* $1,098,139, with CISA (University of Sydney), CSIRO, University of New South Wales, University of Queensland, Griffith University, University of South Australia, Intersect

IBM Smarter Planet Academic Initiatives Award
: *Operations Research for Transportation Course Development* $10,000 (2010), with Guillermo Pineda Villavicencio and Nadezda Sukorukova

UB--Deakin Collaboration Grant
:  $136,635 (2009)

 
# Publications

::: {#refs}
See my papers on [arXiv](https://arxiv.org/a/ugon_j_1.html) or on [my ORCID profile](https://orcid.org/0000-0001-5290-8051).
:::

# Contact

Email
:   <julien.ugon@deakin.edu.au>

