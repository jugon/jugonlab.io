unique.field {doi}
unique.field {url}
unique.field {eprint}

check.double = on

new.entry.type {Thesis}

sort = on
sort.format = {(date){%4d(date)}{}:(year){%4d(year)}{}:(eprint){%d(eprint)}{}:(author){%N(author)}{%N(editor)}:%s(title)}

sort.order {* = author # title # year # note # volume # issue # pages # issn # isbn # doi}

key.number.separator=":"
key.format = {(author){%-1n(author)}{%-1n(editor)}:(date){%d(date)}{(year){%d(year)}{nd}}}

select.non{$type "\(PhD\)?Thesis"}

delete.field{month # usera # userb # userc}
rewrite.rule { Keywords # ",*CV" # "" }

# Enforce "{...}" around fields
rewrite.rule {"^\"\([^#]*\)\"$" "{\1}"}
rewrite.rule {"^ *\([0-9]*\) *$" "{\1}"}

# Remove empty fields
rewrite.rule {"^{ *}$"}

# Unify page range separator
rewrite.rule {pages "\([0-9]+\) *\(-\|---\|–\) *\([0-9]+\)" "\1--\3"}

# Remove the version of the arXiv paper
rewrite.rule {eprint "v\d*" ""}

# Check that 1800<=year<=2029
check.rule { year "^[\"{]1[89][0-9][0-9][\"}]$" }
check.rule { year "^[\"{]20[0-2][0-9][\"}]$" }
check.error.rule { year "" "\@ \$: Year has to be a suitable number" }

# Check that the doi field is not a URL
check.error.rule { doi "\://" "\@ \$: doi field should not be a URL" }

# Check that the url field is not a DOI
check.error.rule { url "doi\.org/" "\@ \$: url field should not contain a doi. Use the doi field instead" }

print.use.tab = off
